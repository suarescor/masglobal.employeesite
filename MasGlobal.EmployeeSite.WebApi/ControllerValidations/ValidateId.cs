﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MasGlobal.EmployeeSite.WebApi.ControllerValidations
{
    public class ValidateId : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string sentId = actionContext.ModelState.Values.ToArray()[0].Value.AttemptedValue;
            int id;
            if(!int.TryParse(sentId, out id) || id < 0)
            {
                actionContext.ModelState.AddModelError("Bad_Id", "Please make sure Id is a positive numeric value");
            }

            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext
                                         .Request
                                         .CreateErrorResponse(HttpStatusCode.BadRequest,
                                                              actionContext.ModelState);
            }
        }
    }
}