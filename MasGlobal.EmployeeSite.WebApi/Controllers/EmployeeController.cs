﻿using MasGlobal.EmployeeSite.Services.Dto;
using MasGlobal.EmployeeSite.Services.Interfases;
using MasGlobal.EmployeeSite.WebApi.ControllerValidations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MasGlobal.EmployeeSite.WebApi.Controllers
{
    public class EmployeeController : ApiController
    {
        IEmployeeService service;

        public EmployeeController(IEmployeeService service)
        {
            this.service = service;
        }

        public IList<EmployeeOutputDto> GetAllEmployees()
        {
            return service.GetAllEmployees();
        }

        [ValidateId]
        public IList<EmployeeOutputDto> GetEmployeeById(int id)
        {
            List<EmployeeOutputDto> employee = new List<EmployeeOutputDto>();
            var result = service.GetEmployeeById(id);
            if(result != null)
            {
                employee.Add(result);
            }
            return employee;
        }
    }
}
