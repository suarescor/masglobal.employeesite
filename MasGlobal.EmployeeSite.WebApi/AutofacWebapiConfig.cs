﻿using Autofac;
using Autofac.Integration.WebApi;
using MasGlobal.EmployeeSite.Business.BusinessLayer;
using MasGlobal.EmployeeSite.Business.Interfases;
using MasGlobal.EmployeeSite.Repository.Interfases;
using MasGlobal.EmployeeSite.Repository.Repository;
using MasGlobal.EmployeeSite.Services.Interfases;
using MasGlobal.EmployeeSite.Services.ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace AutoFacWithWebAPI.App_Start
{
    public class AutofacWebapiConfig
    {

        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }


        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            //Register your Web API controllers.  
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<EmployeeService>()
                   .As<IEmployeeService>()
                   .InstancePerRequest();

            builder.RegisterType<EmployeeRepository>()
                   .As<IEmployeeRepository>()
                   .InstancePerRequest();

            builder.RegisterType<EmployeeBL>()
                   .As<IEmployeeBL>()
                   .InstancePerRequest();

            //Set the dependency resolver to be Autofac.  
            Container = builder.Build();

            return Container;
        }

    }
}