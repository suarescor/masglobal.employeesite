﻿using MasGlobal.EmployeeSite.Domain.BaseClasses;
using MasGlobal.EmployeeSite.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.EmployeeSite.Services.Mappers
{
    public static class EmployeeMapper
    {
        public static EmployeeOutputDto EntityToDto(EmployeeBase employee)
        {
            EmployeeOutputDto response = new EmployeeOutputDto();
            response.Id = employee.id;
            response.AnnualSalary = employee.GetAnnualSalary();
            response.ContractTypeName = employee.contractTypeName.Replace("SalaryEmployee", "");
            response.Name = employee.name;
            response.RoleName = employee.roleName;
            return response;
        }
    }
}
