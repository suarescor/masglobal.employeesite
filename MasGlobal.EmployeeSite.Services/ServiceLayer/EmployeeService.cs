﻿using MasGlobal.EmployeeSite.Services.Interfases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasGlobal.EmployeeSite.Services.Dto;
using MasGlobal.EmployeeSite.Business.Interfases;
using MasGlobal.EmployeeSite.Services.Mappers;

namespace MasGlobal.EmployeeSite.Services.ServiceLayer
{
    public class EmployeeService : IEmployeeService
    {
        IEmployeeBL business;

        public EmployeeService(IEmployeeBL business)
        {
            this.business = business;
        }
        public IList<EmployeeOutputDto> GetAllEmployees()
        {
            var result = business.GetAllEmployees();
            var response = new List<EmployeeOutputDto>();
            if(result != null && result.Count > 0)
            {
                foreach (var employee in result)
                {
                    response.Add(EmployeeMapper.EntityToDto(employee));
                }
            }
            return response;
        }

        public EmployeeOutputDto GetEmployeeById(int id)
        {
            var result = business.GetEmployeeById(id);
            if (result != null)
            {
                var response = EmployeeMapper.EntityToDto(result);
                return response;
            }
            return null;
        }
    }
}
