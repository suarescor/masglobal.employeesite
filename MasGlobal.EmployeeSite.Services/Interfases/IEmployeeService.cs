﻿using MasGlobal.EmployeeSite.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.EmployeeSite.Services.Interfases
{
    public interface IEmployeeService
    {
        EmployeeOutputDto GetEmployeeById(int id);
        IList<EmployeeOutputDto> GetAllEmployees();
    }
}
