﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MasGlobal.EmployeeSite.Business.BusinessLayer;
using MasGlobal.EmployeeSite.Repository.Interfases;
using Moq;
using MasGlobal.EmployeeSite.Domain.Entities;

namespace MasGlobal.EmployeeSite.WebApi.Tests.BusinessLayer
{
    [TestClass]
    public class EmployeeBLTest
    {
        [TestMethod]
        public void TestEmployeeBL_OK()
        {
            var repositoryMock = new Mock<IEmployeeRepository>();
            repositoryMock.Setup(x => x.GetEmployeeById(It.IsAny<int>())).Returns(GetSingleEmployee());
            EmployeeBL bl = new EmployeeBL(repositoryMock.Object);
            var response = bl.GetEmployeeById(1);
            Assert.IsNotNull(response);
            Assert.AreEqual(response.id, 1);
            Assert.IsInstanceOfType(response, typeof(HourlySalaryEmployee));
        }

        private EmployeeJsonDto GetSingleEmployee()
        {
            return new EmployeeJsonDto()
            {
                id = 1,
                name = "Juan",
                contractTypeName = "HourlySalaryEmployee",
                roleId = 1,
                roleName = "Administrator",
                roleDescription = null,
                hourlySalary = 60000,
                monthlySalary = 80000
            };
        }
    }
}
