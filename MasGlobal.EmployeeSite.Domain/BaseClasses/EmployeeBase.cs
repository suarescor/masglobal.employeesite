﻿using MasGlobal.EmployeeSite.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.EmployeeSite.Domain.BaseClasses
{
    public abstract class EmployeeBase
    {
        public int id { get; set; }
        public string name { get; set; }
        public string contractTypeName { get; set; }
        public int roleId { get; set; }
        public string roleName { get; set; }
        public object roleDescription { get; set; }
        public double hourlySalary { get; set; }
        public double monthlySalary { get; set; }

        public abstract double GetAnnualSalary();
    }
}

