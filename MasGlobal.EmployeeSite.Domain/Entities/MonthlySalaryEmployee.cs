﻿using MasGlobal.EmployeeSite.Domain.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.EmployeeSite.Domain.Entities
{
    public class MonthlySalaryEmployee : EmployeeBase
    {
        public override double GetAnnualSalary()
        {
            return this.monthlySalary * 12;
        }
    }
}
