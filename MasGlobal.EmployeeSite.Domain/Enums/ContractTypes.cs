﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.EmployeeSite.Domain.Enums
{
    public enum ContractTypes
    {
        MonthlySalaryEmployee,
        HourlySalaryEmployee
    }
}
