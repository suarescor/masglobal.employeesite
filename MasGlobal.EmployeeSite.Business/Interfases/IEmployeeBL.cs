﻿using MasGlobal.EmployeeSite.Domain.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.EmployeeSite.Business.Interfases
{
    public interface IEmployeeBL
    {
        EmployeeBase GetEmployeeById(int Id);
        IList<EmployeeBase> GetAllEmployees();
    }
}
