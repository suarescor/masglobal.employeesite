﻿using MasGlobal.EmployeeSite.Domain.BaseClasses;
using MasGlobal.EmployeeSite.Domain.Entities;
using MasGlobal.EmployeeSite.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.EmployeeSite.Business.Factory
{
    public sealed class EmployeeFactory
    {

        private static readonly Lazy<EmployeeFactory>
            lazy =
            new Lazy<EmployeeFactory>
                (() => new EmployeeFactory());

        public static EmployeeFactory Instance { get { return lazy.Value; } }

        private EmployeeFactory()
        {
        }

        public IList<EmployeeBase> CreateEmployeeList(IList<EmployeeJsonDto> jsonEmployees)
        {
            IList<EmployeeBase> employees = new List<EmployeeBase>();

            foreach (var row in jsonEmployees)
            {
                ContractTypes type = (ContractTypes)Enum.Parse(typeof(ContractTypes), row.contractTypeName);
                switch (type)
                {
                    case ContractTypes.HourlySalaryEmployee:
                        employees.Add(new HourlySalaryEmployee()
                        {
                            id = row.id,
                            contractTypeName = row.contractTypeName,
                            name = row.name,
                            roleDescription = row.roleDescription,
                            roleId = row.roleId,
                            roleName = row.roleName,
                            hourlySalary = row.hourlySalary,
                            monthlySalary = row.monthlySalary
                        });
                        break;
                    case ContractTypes.MonthlySalaryEmployee:
                        employees.Add(new MonthlySalaryEmployee()
                        {
                            id = row.id,
                            contractTypeName = row.contractTypeName,
                            name = row.name,
                            roleDescription = row.roleDescription,
                            roleId = row.roleId,
                            roleName = row.roleName,
                            hourlySalary = row.hourlySalary,
                            monthlySalary = row.monthlySalary
                        });
                        break;
                    default:
                        throw new ArgumentNullException();
                }
            }

            return employees;
        }


    }
}
