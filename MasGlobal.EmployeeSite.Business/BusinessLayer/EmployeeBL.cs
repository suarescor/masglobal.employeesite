﻿using MasGlobal.EmployeeSite.Business.Interfases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasGlobal.EmployeeSite.Domain.BaseClasses;
using MasGlobal.EmployeeSite.Repository.Interfases;
using MasGlobal.EmployeeSite.Business.Factory;
using MasGlobal.EmployeeSite.Domain.Entities;

namespace MasGlobal.EmployeeSite.Business.BusinessLayer
{
    public class EmployeeBL : IEmployeeBL
    {
        IEmployeeRepository repository;

        public EmployeeBL(IEmployeeRepository repository)
        {
            this.repository = repository;
        }
        public IList<EmployeeBase> GetAllEmployees()
        {
            var response = new List<EmployeeBase>();
            var result = repository.GetAllEmployees();
            if(result != null)
            {
                response = EmployeeFactory.Instance.CreateEmployeeList(result).ToList();
            }
            return response;
        }

        public EmployeeBase GetEmployeeById(int id)
        {
            var singleResult = repository.GetEmployeeById(id);
            if (singleResult != null)
            {
                return EmployeeFactory.Instance.CreateEmployeeList(new List<EmployeeJsonDto>() { singleResult }).FirstOrDefault();
            }
            return null;
        }
    }
}
