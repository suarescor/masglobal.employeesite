﻿using MasGlobal.EmployeeSite.Repository.Interfases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasGlobal.EmployeeSite.Domain.Entities;
using MasGlobal.EmployeeSite.Common.Helpers;
using System.Web.Configuration;
using Newtonsoft.Json;

namespace MasGlobal.EmployeeSite.Repository.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private string URL;
        public EmployeeRepository()
        {
            URL = WebConfigurationManager.AppSettings["employeeUrl"];
        }
        public IList<EmployeeJsonDto> GetAllEmployees()
        {
            IList<EmployeeJsonDto> employees = new List<EmployeeJsonDto>();
            var result = HttpRequestHelper.Get(URL);
            if (!string.IsNullOrEmpty(result))
            {
                employees = JsonConvert.DeserializeObject<List<EmployeeJsonDto>>(result);
            }
            return employees;
        }

        public EmployeeJsonDto GetEmployeeById(int Id)
        {
            return GetAllEmployees().Where(x => x.id == Id).FirstOrDefault();
        }
    }
}
