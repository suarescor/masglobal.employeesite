﻿using MasGlobal.EmployeeSite.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.EmployeeSite.Repository.Interfases
{
    public interface IEmployeeRepository
    {
        IList<EmployeeJsonDto> GetAllEmployees();
        EmployeeJsonDto GetEmployeeById(int Id);
    }
}
